import psycopg2
from conexion import dc
conexion = psycopg2.connect(**dc)
cursor = conexion.cursor()

print('CREAR USER STORY')
sql = 'insert into userstorie(id, codigo, nombre, card, conversation, confirmation, idproyecto) values (%s,%s,%s, %s, %s, %s, %s)'
id=input ('Digite el id del User Story: ')
codigo=input ('Digite el código del User Story: ')
nombre=input ('Digite el nombre del User Story: ')
card=input ('Escriba el card del User Story: ')
conversation=input ('Escriba el conversation del User Story: ')
confirmation=input ('Escriba el confirmation del User Story: ')
idproyecto=input ('Digite iD del proyecto al que pertenece este User Story: ')
parametros=(id, codigo, nombre, card, conversation, confirmation, idproyecto)

longitud_codigo=len(codigo)
longitud_nombre=len(nombre) 
longitud_id_proyecto=len(id) 

if longitud_codigo==0:
        print('El código del User Story no puede estar vacío')
        cursor.close()
        conexion.close()
elif longitud_nombre==0:
        print('El nombre del User Story no puede estar vacío')
        cursor.close()
        conexion.close()
elif longitud_id_proyecto==0:
        print('El iD del proyecto no puede estar vacío')
        cursor.close()
        conexion.close()
else: 
    try: 
        cursor.execute(sql, parametros)
        conexion.commit()
        print('Se han guardado los datos exitosamente')
    except:    
        print('hubo un error, intente más tarde')
    finally:
        cursor.close()
        conexion.close()
