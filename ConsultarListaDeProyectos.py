import psycopg2
from conexion import dc
conexion = psycopg2.connect(**dc) 
cursor = conexion.cursor()
cursor.execute('select nombre,descripcion from proyecto')
registros = cursor.fetchall()
print('CONSULTAR LISTA DE PROYECTOS:')
for registro in registros:
    nombre=registro[0]
    descripcion=registro[1]
    print(f'{nombre} Descripción: {descripcion}')
cursor.close()
conexion.close()