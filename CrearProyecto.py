import psycopg2
from conexion import dc 
conexion = psycopg2.connect(**dc) 
cursor = conexion.cursor()

print('CREAR PROYECTO')

sql = 'insert into proyecto(id, nombre, descripcion) values (%s,%s,%s)'
id=input ('Digite el id del proyecto: ')
nombre=input ('Digite el nombre del proyecto: ')
descripcion=input ('Digite la descripcion del proyecto: ')
parametros=(id, nombre, descripcion)

longitud_nombre=len(nombre) 
longitud_id=len(id) 

if longitud_nombre==0:
        print('El nombre del proyecto no puede estar vacío')
elif longitud_id==0:
        print('El id del proyecto no puede estar vacío')
else: 
    cursor.execute(sql, parametros)
    conexion.commit()
    print('Se han guardado los datos exitosamente')


cursor.close()
conexion.close()
    